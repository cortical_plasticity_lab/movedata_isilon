# MoveData_Isilon

** Matlab (R2017a) code for batch conversion of Intan or TDT raw electrophysiological recordings into single-channel *.mat files in the stereotyped format used by CPL. **

## Description

Code is specialized for parallel use of cores on the University of Kansas Medical Center (KUMC) Isilon supercluster. The current server distribution of Matlab is R2017a.
This code is run to extract code from other file formats into Matlab-compatible format, in a stereotyped "Block" hierarchy that contains sub-folders in a similar
naming format/convention to that used in SpyCode.

## Sub-directories

** Intan **
Code is specialized for use with files recorded when acquiring neurophysiological data using the Intan Evaluation Board and RHD2000 series chips/headstages, or 
the Intan Stimulation Controller and RHS2000 series chips/headstages.

** MicroD **
Code specialized for extraction from recordings made using the custom ASIC developed in collaboration with Case Western University.

** TDT **
Code specialized for extraction from recordings made using the Tucker-Davis Technologies (TDT) system.